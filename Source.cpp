#include <iostream>
#include "Matrix.h"

using namespace std;

// disregard this function - will put it in class  later
int *multiplyMx(int *mx1, int *mx2, int nRow, int nCol, int wdth) {
	int *pdt = new int[nRow * nCol], *rtr = pdt, i, j, k, *mx2_tmp, *mx1_tmp;

	for (i = nRow; i--; mx1 += nRow, mx2 -= nCol) { // for each row of mx1 
		for (j = nCol; j--; mx2++, pdt++) { // for each colum of mx2
			for (k = wdth, mx2_tmp = mx2, mx1_tmp = mx1, *pdt = 0; k--; mx2_tmp += nCol, mx1_tmp++) {
				*pdt += *mx1_tmp * *mx2_tmp; // for each element of row / colum of mx1 / mx2
			}
		}
	}
	return rtr;
}


void main() 
{
	int m1[2][2] = { {1,2}, {3,4} };
	
	Matrix<int> example_one(2, 2, *m1); 
	Matrix<int> example_two(example_one);

	example_one.print();
	example_one.add(example_two);
	example_one.print();
	cout << example_one.getIJ(1,1);

		//// nRow will be the number of rows in a new matrix, and nCol will be the number of colums in that matrix
		//const int nRow = 4, wdth = 3, nCol = 2;

		//int mx1[nRow][nRow] = {
		//	{ 1,6,3 },
		//	{ 6,2,7 },
		//	{ 8,7,3 },
		//	{ 4,2,3 }
		//};

		//int mx2[nRow][nCol] = {
		//	{ 1,6 },
		//	{ 6,2 },
		//	{ 3,7 }
		//};

		////  Create a pointer to a new array, whose size will be nRow * nCol, and call the function multiplying 2 matrices
		//int *product = multiplyMx(mx1[0], mx2[0], nRow, nCol, wdth);

		//// Print the product of two matrices
		//for (int i = nRow, j; i--; cout << endl)
		//	for (j = nCol; j--;) cout << *product++ << ' ';

		//product -= nCol*nRow;
		//delete[] product, product = NULL;


	system("pause");
}