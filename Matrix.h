#pragma once
#include <iostream>
#include <assert.h>

template <class T> 
class Matrix {
private:
	int   _row;
	int   _col; 
	T  ** _matrix;
public:
	Matrix();
	Matrix(int, int);
	Matrix(int, int, T*);
	Matrix(Matrix const &mtx);

	void minus_one();
	void add(Matrix<T> const &mtx);
	void multiply(Matrix<T> const &mtx);
	void transpon();

	T getIJ(int i, int j) const;
	void print() const;

};

template <class T>
Matrix<T>::Matrix()
{
	_row = 2;
	_col = 2;
	_matrix = new T*[_row];

	T **p = _matrix;

	for (int i = 0; i < _row; i++, p++)
		*p = new T[_col];
}

template <class T>
Matrix<T>::Matrix(int row, int col)
{
	_row = row;
	_col = col;
	_matrix = new T*[_row];

	T **p = _matrix;

	for (int i = 0; i < _row; i++, p++)
		*p = new T[_col];
}

template <class T>
Matrix<T>::Matrix(Matrix const &m)
{
	_row = m._row;
	_col = m._col;
	_matrix = new T*[_row];

	T **p1 = _matrix, **p2 = m._matrix, *p3, *p4;

	for (int i = 0, j; i++ < _row; p1++, p2++)
	{
		*p1 = new T[_col];
		for (j = 0, p3 = *p1, p4 = *p2; j++ < _col; p3++, p4++)
		{
			*p3 = *p4;
		}
	}
}

template <class T>
Matrix<T>::Matrix(int row, int col, T*m)
{
	_row = row;
	_col = col;
	_matrix = new T*[_row];

	T **p1 = _matrix, *p2, *p3 = m;

	for (int i = 0, j; i++ < _row; p1++)
	{
		*p1 = new T[_col];
		for (j = 0, p2 = *p1; j++ < _col; p2++, p3++)
		{
			*p2 = *p3;
		}
	}
}

template <class T>
void Matrix<T>::print() const
{
	T **row = _matrix, *col;
	for (int i = 0, j; i++ < _row; row++)
	{
		for (j = 0, col = *row; j++ < _col; col++)
		{
			std::cout << *col << ' ';
		}
		std::cout << "\n";
	}
}

template <class T>
void Matrix<T>::minus_one() 
{
	T **p1 = _matrix, *p2;
	for (int i = 0, j; i++ < _row; p1++)
		for (j = 0, p2 = *p1; j++ < _col; p2++)
			-*p2;

}

template <class T>
void Matrix<T>::add(Matrix<T> const &mtx)
{  
	assert(_row == mtx._row && _col == mtx._col);

	T **m1_row = _matrix, *m1_col, **m2_row = mtx._matrix, *m2_col;

	for (int i = 0, j; i++ < _row; m1_row++, m2_row++)
		for (j = 0, m1_col = *m1_row, m2_col = *m1_row; j++ < _col; m1_col++, m2_col++)
			*m1_col += *m2_col;

}

template <class T>
T Matrix<T>::getIJ(int i, int j) const
{
	assert(_row > i && _col > j);
	return *(*(_matrix + i)+j);
}

template <class T>
void Matrix<T>::transpon()
{
	T tmp;
	if (_col == _row) {
		for (int i = 0, j; i < _row; i++) {
			for (j = i; j < _col; j++) {
				tmp = _matrix[i][j];
				_matrix[i][j] = _matrix[j][i];
				_matrix[j][i] = tmp;
			}
		}
	}

}

template <class T>
void Matrix<T>::multiply(Matrix<T> const &mtx)
{
	// in process 

}